<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Buku;
use App\Kategori;
use App\Pencipta;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $buku = DB::table('table_buku')
        ->join('table_kategori', 'table_buku.kategori', '=', 'table_kategori.kategori')
        ->join('table_pencipta', 'table_buku.pencipta', '=', 'table_pencipta.id')
        ->select('table_buku.id_buku','table_buku.judul_buku', 'table_buku.deskripsi', 
        'table_kategori.deskripsi as kategori', 'table_pencipta.nama as pencipta')
        ->get();
        // return $buku;
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $kategori = Kategori::all();
        $pencipta = Pencipta::all();
        return view('buku.create', array('kategori' => $kategori,
        'pencipta' => $pencipta));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $this->validate($request, [ 
            'judul_buku' => 'required', 
            'deskripsi'  => 'required',
            'pencipta'  => 'required',
            'kategori'  => 'required',
        ]);
        $buku = new Buku;
        $buku->judul_buku = $request->judul_buku;
        $buku->deskripsi  = $request->deskripsi;
        $buku->kategori   = $request->kategori;
        $buku->pencipta = $request->pencipta;
        $buku->save();
        return redirect('buku')->with('msg','Data Berhasil di Simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $buku = Buku::where('id_buku', $id)->first();
        return $buku;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $kategori = Kategori::all();
        $pencipta = Pencipta::all();
        $buku = DB::table('table_buku')
        ->join('table_kategori', 'table_buku.kategori', '=', 'table_kategori.kategori')
        ->join('table_pencipta', 'table_buku.pencipta', '=', 'table_pencipta.id')
        ->select('table_buku.id_buku as id_buku','table_buku.judul_buku as judul_buku', 'table_buku.deskripsi as buku_desk',
        'table_kategori.deskripsi as kategori','table_pencipta.nama as pencipta')
        ->where('table_buku.id_buku', '=', $id)->first();
        // die($kategori);
        return view('buku.edit', array(
            'buku' => $buku,
            'pencipta' => $pencipta,
            'kategori' => $kategori
        ));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Buku::where('id_buku',$id)
        ->update([
            'judul_buku' => $request->judul_buku,
            'deskripsi' => $request->deskripsi,
            'kategori' => $request->kategori,
            'pencipta' => $request->pencipta,
        ]);
        return redirect('buku')->with('msg','Informasi Buku Telah Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletedRows = Buku::where('id_buku', $id)->delete();
        return redirect('buku')->with('msg','Informasi Buku Telah Dihapus'.$deletedRows);
    }
}
