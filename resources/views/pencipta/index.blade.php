@extends('layouts.app')


@section('title', 'Laravel - SI Perpustakaan')


@section('content')


<div class="container">


<div class="jumbotron">


@if(session('msg'))


<div class="alert alert-success alert-dismissible fade show mt-2" 


            role="alert">


{{session('msg')}}


<button type="button" class="close" data-dismiss="alert" 


                 aria-label="Close">


<span aria-hidden="true">&times;</span>


</button>


</div>


@endif


<h1 class="display-6">Pencipta Buku</h1>


<hr class="my-4">     


<a href="pencipta/create" class="btn btn-primary mb-1">


Tambah Pencipta Buku</a>       


<table class="table">


<thead class="thead-dark">


<tr>


<th scope="col">#</th>


<th scope="col">Nama</th>


<th></th>


</tr>


</thead>


<tbody>


@foreach ($pencipta as $kat)


<tr>


<td>{{ $loop->iteration }}</td>


<td>{{ $kat->nama }}</td>


<td>


<a href="pencipta/edit/{{ $kat->id }}" class="badge badge-primary">Edit</a>


<a href="pencipta/destroy/{{ $kat->id }}" class="badge badge-danger">Hapus</a>


</td>


</tr>


@endforeach


</tbody>


</table>


</div>


</div>


@endsection