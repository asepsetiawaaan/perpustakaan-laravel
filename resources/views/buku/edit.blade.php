@extends('layouts.app')
@section('title', 'Laravel - SI Perpustakaan')
@section('content')
<div class="container">
<div class="jumbotron">
<h1 class="display-6">Edit Data Buku</h1>
<hr class="my-4">     
<form action="{{ action('BukuController@update', $buku->id_buku) }}" method="POST" enctype="multipart/form-data">
@csrf
<div class="form-group">
<label for="judul_buku">Judul Buku</label>


<input type="text" class="form-control" name="judul_buku" 


                    placeholder="Judul Buku" value="{{ $buku->judul_buku }}">


</div>


<div class="form-group">


<label for="deskripsi">Deskripsi Buku</label>


<input type="text" class="form-control" name="deskripsi" 


                    placeholder="Deskripsi Buku" value="{{ $buku->buku_desk }}">


</div>


<div class="form-group">


<label for="kategori">Kategori Buku</label>


<select class="form-control" id="kategori" name="kategori">


@foreach ($kategori as $kat)
    @if($kat->deskripsi == $buku->kategori)
        <option value="{{ $kat->kategori }}" selected="selected">{{ $kat->deskripsi }}</option>        
    @else
        <option value="{{ $kat->kategori }}">{{ $kat->deskripsi }}</option>
    @endif


@endforeach


</select>


</div>
<div class="form-group">


<label for="pencipta">Pencipta Buku</label>


<select class="form-control" id="pencipta" name="pencipta">


@foreach ($pencipta as $kat)
    @if($kat->id == $buku->pencipta)
        <option value="{{ $kat->id }}" selected="selected">

        {{ $kat->nama }}</option>
    @else
    <option value="{{ $kat->id }}">
        {{ $kat->nama }}</option>
    @endif

@endforeach


</select>


</div>


<div class="form-group">




</div>


<button type="submit" class="btn btn-primary">Simpan</button>


</form>


</div>


</div>


@endsection