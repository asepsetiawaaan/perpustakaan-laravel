<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pencipta extends Model
{
    protected $table = 'table_pencipta';
    protected $fillable = ['nama'];
}
