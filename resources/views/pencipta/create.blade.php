@extends('layouts.app')


@section('title', 'Laravel - SI Perpustakaan')


@section('content')


<div class="container">


<div class="jumbotron">


<h1 class="display-6">Tambah Pencipta Buku</h1>


<hr class="my-4">     


<form action="/pencipta" method="POST">


@csrf


<div class="form-group">


<label for="deskripsi">Nama</label>


<input type="text" class="form-control" id="nama" 


                    name="nama" placeholder="nama" 


                    value="{{ old('nama') }}">


</div>


<button type="submit" class="btn btn-primary">Simpan</button>


</form>


</div>


</div>


@endsection