<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pencipta;

class PenciptaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pencipta = Pencipta::all();
        return view('pencipta/index', compact('pencipta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pencipta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Pencipta::create($request->all());
        return redirect('pencipta')->with('msg','Data Berhasil di Simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pencipta = Pencipta::where('id', $id)->first();
        return view('pencipta.edit', compact('pencipta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Pencipta::where('id',$id)
        ->update([
            'nama' => $request->nama,
        ]);
        return redirect('pencipta')->with('msg','Informasi Buku Telah Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletedRows = Pencipta::where('id', $id)->delete();
        return redirect('pencipta')->with('msg','Informasi Pencipta Telah Dihapus'.$deletedRows);
    }
}
