<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('kategori','KategoriController');

Route::get('kategori/edit/{id}', 'KategoriController@edit');
Route::post('kategori/update/{id}', 'KategoriController@update');
Route::get('kategori/destroy/{id}', 'KategoriController@destroy');

Route::resource('buku','BukuController');

Route::resource('pencipta','PenciptaController');
Route::get('pencipta/edit/{id}', 'PenciptaController@edit');
Route::post('pencipta/update/{id}', 'PenciptaController@update');
Route::get('pencipta/destroy/{id}', 'PenciptaController@destroy');

Route::get('buku/edit/{id}', 'BukuController@edit');

Route::post('/buku/update/{id}', 'BukuController@update');

Route::get('/buku/destroy/{id}', 'BukuController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
